FROM alpine:edge

ENV S6_OVERLAY_VERSION v1.21.4.0

# Terminate all s6 childs when init stage fails
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

# Make some room for our friend flood and his older brother rtorrent
ENV RUTORRENT_HOME /var/www/rutorrent
ENV RTORRENT_HOME /home/rtorrent

# Install main packages and build-dependencies and do some other stuff
RUN addgroup rtorrent \
    && adduser -D -s /bin/false -h "$RTORRENT_HOME" -G rtorrent rtorrent \
    && apk add --update --no-cache --no-progress bash coreutils curl ffmpeg gzip mediainfo nginx php7 php7-fpm php7-json php7-mbstring rtorrent sox su-exec unrar unzip wget \
    && apk add --update --no-cache --no-progress --virtual .build-deps git \
    && wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz \
    && tar -C / -xzvf s6-overlay-amd64.tar.gz \
    && rm s6-overlay-amd64.tar.gz \
    && mkdir /run/rtorrent && chown rtorrent:rtorrent /run/rtorrent/ && chmod 750 /run/rtorrent/ \
    && mkdir /run/php && chown rtorrent:rtorrent /run/php/ && chmod 750 /run/php/ \
    && mkdir /run/nginx && chown rtorrent:rtorrent /run/nginx/ && chmod 750 /run/nginx/ \
    && git clone https://github.com/Novik/ruTorrent "$RUTORRENT_HOME" \
    && git clone https://github.com/ArtyumX/ruTorrent-Themes/ /tmp/themes/ \
    && mv /tmp/themes/* "${RUTORRENT_HOME}/plugins/theme/themes/" && rm -rf /tmp/themes/ \
    && chown -R rtorrent:rtorrent "$RUTORRENT_HOME" \
    && sed -E -e 's/\$scgi_port = 5000;/\$scgi_port = 0;/' \
              -e 's|\$scgi_host = "127.0.0.1";|\$scgi_host = "unix:///run/rtorrent/rtorrent.sock";|' \
              -e "s|\"php\"\s+=> ''.*$|\"php\"   => '/usr/bin/php',|" \
              -e "s|\"curl\"\s+=> ''.*$|\"curl\"  => '/usr/bin/curl',|" \
              -e "s|\"gzip\"\s+=> ''.*$|\"gzip\"  => '/usr/bin/gzip',|" \
              -e "s|\"id\"\s+=> ''.*$|\"id\"    => '/usr/bin/id',|" \
              -e "s|\"stat\"\s+=> ''.*$|\"stat\"  => '/bin/stat',|" \
	          -e "/\"stat\"\s+=>/a\                \"pgrep\" => '/usr/bin/pgrep'," \
              -i "${RUTORRENT_HOME}/conf/config.php" \
    && sed -e 's|;error_log = log/php7/error.log|error_log = /dev/stderr|' -i /etc/php7/php-fpm.conf \
    && sed -E -e 's/^user = nobody$/user = rtorrent/' \
              -e 's/^group = nobody$/group = rtorrent/' \
              -e 's|^listen .*$|listen = /run/php/php7-fpm.sock|' \
              -e 's/^;listen.owner = nobody$/listen.owner = rtorrent/' \
              -e 's/^;listen.group = nobody$/listen.group = rtorrent/' \
              -e 's/^;(listen.mode = 0660)$/\1/' \
              -i /etc/php7/php-fpm.d/www.conf \
    && rm -rf "${RUTORRENT_HOME}/.git" \
    && apk del --purge .build-deps \
    && sed 's/user nginx;/user rtorrent;/' -i /etc/nginx/nginx.conf \
    && ln -sf /dev/stdout /var/log/nginx/access.log \
    && ln -sf /dev/stderr /var/log/nginx/error.log \
    && chown -Rf rtorrent:rtorrent /var/tmp/nginx/ \
    && chown -Rf rtorrent:rtorrent /var/lib/nginx/

# Copy the configuration file of rtorrent and nginx
COPY --chown=rtorrent:rtorrent ./config/.rtorrent.rc $RTORRENT_HOME
COPY ./config/default.conf /etc/nginx/conf.d/default.conf

# Copy required assets for s6-overlay
COPY ./assets/s6-overlay/ /etc/

ENTRYPOINT ["/init"]
EXPOSE 80
HEALTHCHECK --interval=30s --retries=3 CMD curl --fail http://localhost:80 || exit 1
